@echo off

SetLocal EnableExtensions EnableDelayedExpansion

for /f "usebackq skip=1 tokens=1,2 delims= " %%a in (`wmic process where ^(executablepath like "%%chrome.exe%%" and not executablepath like "%%type%%"^) get parentprocessid^, processid`) do call :Fn1 %%a %%b
goto End

:Fn1
if "%2" == "" (
echo Blank Parent ID
) else (
echo Parent %1
wmic process where ^(processid=%1^) get executablepath, processid
echo Killing %2
wmic process where ^(processid=%2^) get executablepath, processid
taskkill /F /PID %2
exit
)
goto :eof


:End