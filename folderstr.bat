@echo off
REM Ref. my mail, search 'careful xcopy list.txt'; https://superuser.com/a/617602
REM for /? ... describes the various ways, the variable gets expanded - awesome in-built feature.

REM rmdir /s c:\code ... s for sub i guess

set /p AREYOUSURE=Have you cleared the target folder and put List.txt having full file paths in current folder? Please confirm (Y/[N])?
if /i "%AREYOUSURE%" NEQ "Y" goto END

for /f "tokens=*" %%a in (List.txt) do md ".%%~pa" & xcopy "%%~a" ".%%~pa"

:END