@echo off

rem 4aug - needs better comments - this is for saving to server w/o affecting stash... have updated it to make a patch since the last commit pushed instead of just the last commit

rem https://stackoverflow.com/questions/6359820/how-to-set-commands-output-as-a-variable-in-a-batch-file
rem https://stackoverflow.com/questions/19131029/how-to-get-date-in-bat-file
rem https://stackoverflow.com/questions/4076239/finding-out-the-name-of-the-original-repository-you-cloned-from-in-git
rem https://stackoverflow.com/questions/949314/how-to-retrieve-the-hash-for-the-current-commit-in-git
rem https://stackoverflow.com/questions/18979120/is-it-possible-to-add-a-comment-to-a-diff-file-unified
rem https://stackoverflow.com/questions/52221570/applying-patch-doesnt-create-newly-created-files-in-git
rem default is "changes you made relative to the index (staging area for the next commit)" https://git-scm.com/docs/git-diff
rem https://stackoverflow.com/questions/132799/how-can-you-echo-a-newline-in-batch-files

echo gitkeep yourMessage

echo this tool assumes you are in a git folder and intend to 'gitkeep' on it.
echo this creates an enriched patch and copies it to z:\GitPatchesStore\ (HOMEDRIVE \\fileshareoce\userss$)

echo To apply the patch, use this command:
echo "git apply <filename>"

pause

git remote show -n origin | grep Push | cut -d/ -f6- > tmpFile 
set /p repoName= < tmpFile 
del tmpFile 

for /f "tokens=1-4 delims=/ " %%i in ("%date%") do (
     set dow=%%i
     set month=%%j
     set day=%%k
     set year=%%l
)
set dateStr=%month%_%day%_%year%

set fileName=%dateStr%_%repoName%.patch
echo %fileName%

echo repo name: > C:\Users\aamat\__MY_DATA\GitPatchesStore\%fileName%
echo 	%repoName% >> C:\Users\aamat\__MY_DATA\GitPatchesStore\%fileName%

echo last commit id: >> C:\Users\aamat\__MY_DATA\GitPatchesStore\%fileName%
git branch --show-current > tmpFile
set /p originBranch= < tmpFile
set originBranch=origin/%originBranch%

git rev-parse %originBranch% > tmpFile
set /p lastCommitId= < tmpFile
del tmpFile
echo 	%lastCommitId% >> C:\Users\aamat\__MY_DATA\GitPatchesStore\%fileName%

echo gitkeep message: >> C:\Users\aamat\__MY_DATA\GitPatchesStore\%fileName%
echo 	%1 >> C:\Users\aamat\__MY_DATA\GitPatchesStore\%fileName%

echo --- --- --- & echo.
echo Writing: git diff %lastCommitId%
git diff %lastCommitId% >> C:\Users\aamat\__MY_DATA\GitPatchesStore\%fileName%

copy C:\Users\aamat\__MY_DATA\GitPatchesStore\%fileName% z:\GitPatchesStore\%fileName%
echo Location: z:\GitPatchesStore\
echo Full path: z:\GitPatchesStore\%fileName%

echo Location: C:\Users\aamat\__MY_DATA\GitPatchesStore
echo Full path: C:\Users\aamat\__MY_DATA\GitPatchesStore\%fileName%

rem TODO
rem add info on which was the actual commit against which the diff was made
rem add 'commit' comment text
rem need to look into reliably applying the patches - cud integrate in this command itself?
