@echo off

rem git fetch origin <take an older commit id under master>:refs/remotes/origin/master
rem git fetch origin +refs/heads/*.Net_4.10*:refs/remotes/origin/*.Net_4.10*

rem set /p AREYOUSURE=Have you cleared the target folder and put List.txt having full file paths in current folder? Please confirm (Y/[N])?
rem if /i "%AREYOUSURE%" NEQ "Y" goto END
echo %0
echo %~dP0
for /f "tokens=*" %%a in (%~dP0\RepoFullPathList.txt) do echo %%a & echo. &  git -C %%a fetch --all --progress -v >> gitFetchSchedOutput.txt
pause

:END