@echo off

echo "push all changes required to be saved in the existing local repo"
echo "npp %mypath%/bfg_cust.bat"
echo.
echo "cd %userprofile%\Downloads"
echo "mkdir git_bfg && cd git_bfg && git clone --mirror https://vyoam@bitbucket.org/vyoam/vertxtrainingjune2019.git"
echo "cd vertxtrainingjune2019.git"

echo "exit if not done above, else press a key..."

pause

echo "command 1 >>> >>> >>>"
echo.

java -jar %mypath%/bfg.jar --delete-files {DBRead,DBWrite,BookInfo}Verticle.java

echo "command 2 >>> >>> >>>"
echo.

git reflog expire --expire=now --all && git gc --prune=now --aggressive

echo "you can git push manually to complete!!! then delete the above folder. Then in orig cloned local repo, fetch all refs and revert to origin/master HEAD ... or cud just clone afresh"

rem *** https://rtyley.github.io/bfg-repo-cleaner/ - has jar link
rem * https://www.polyu.edu.hk/its/general-information/newsletter/109-year-2016/mar-16/508-how-to-use-github-without-leaking-your-credentials https://www.polyu.edu.hk/its/general-information/newsletter/109-year-2016/mar-16/508-how-to-use-github-without-leaking-your-credentials ... https://github.com/IBM/BluePic/wiki/Using-BFG-Repo-Cleaner-tool-to-remove-sensitive-files-from-your-git-repo

rem * https://stackoverflow.com/questions/3959924/whats-the-difference-between-git-clone-mirror-and-git-clone-bare


rem * Reduce repository size - Atlassian Documentation
rem * https://confluence.atlassian.com/bitbucket/reduce-repository-size-321848262.html#Reducerepositorysize-Garbagecollectingdeaddata
rem * 
rem *** site / master / issues / #11593 - Allow users to mark repositories for git gc (BB-13894) — Bitbucket
rem * https://bitbucket.org/site/master/issues/11593/allow-users-to-mark-repositories-for-git
rem * 
rem * git - BFG Repo Cleaner - Deleted Files Still In Commit Changes - Stack Overflow
rem * https://stackoverflow.com/questions/55366996/bfg-repo-cleaner-deleted-files-still-in-commit-changes
rem * 
rem * Size of the Repo Increased · Issue #68 · rtyley/bfg-repo-cleaner
rem * https://github.com/rtyley/bfg-repo-cleaner/issues/68#issuecomment-191117240

rem *** https://stackoverflow.com/questions/9135095/git-gc-aggressive-push-to-server

rem unfortunately, will have to ask support to to 'git gc' to remove these older commit urls... tho good news is, these are completely hidden apart from the direct links... so pwd etc. are not visible publically w/o direct links
rem *
rem * oam / VertXTrainingJune2019 / commit / 8383690f382e — Bitbucket
rem * https://bitbucket.org/vyoam/vertxtrainingjune2019/commits/8383690f382ec678de1f59be80ac2e4ad52bfd92
rem * 
rem * vyoam / VertXTrainingJune2019 / commit / 5b8901f982da — Bitbucket
rem * https://bitbucket.org/vyoam/vertxtrainingjune2019/commits/5b8901f982da10e2cd76153058b949744ae4be8f
rem * 
rem * vyoam / VertXTrainingJune2019 / commit / bccd9164ab92 — Bitbucket
rem * https://bitbucket.org/vyoam/vertxtrainingjune2019/commits/bccd9164ab9269cb66abe9d3162ce3bb26a908ed#chg-src/main/java/com/visa/training_proj/DBReadVerticle.java
rem * 
rem * vyoam / VertXTrainingJune2019 / commit / 1f1f5e6e77cf — Bitbucket
rem * https://bitbucket.org/vyoam/vertxtrainingjune2019/commits/1f1f5e6e77cf7626f78789daa6c35392ed32f060
rem * 
rem * vyoam / VertXTrainingJune2019 / commit / aa8e5f07a5f9 — Bitbucket
rem * https://bitbucket.org/vyoam/vertxtrainingjune2019/commits/aa8e5f07a5f967f8e12e6a0f4a8c8a9ac95a1ebd
rem * 
rem * vyoam / VertXTrainingJune2019 / commit / c7549f2ccf7b — Bitbucket
rem * https://bitbucket.org/vyoam/vertxtrainingjune2019/commits/c7549f2ccf7bbbebd67fdc8a6f15d185a6b996b7
rem * 
rem * vyoam / VertXTrainingJune2019 / commit / 2d13c30b4ec5 — Bitbucket
rem * https://bitbucket.org/vyoam/vertxtrainingjune2019/commits/2d13c30b4ec57513f7bb9cff0413d12b3470c87e
rem * 
rem * vyoam / VertXTrainingJune2019 / commit / 4d6f2240ffa4 — Bitbucket
rem * https://bitbucket.org/vyoam/vertxtrainingjune2019/commits/4d6f2240ffa4fc5a93448855bfed341c4177173d
