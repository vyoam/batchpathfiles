@echo off

SetLocal EnableExtensions EnableDelayedExpansion

:loop
for /f "usebackq skip=1 tokens=1 delims= " %%a in (`wmic process where ^(executablepath like "%%iexplore.exe%%"^) get processid`) do taskkill /F /PID %%a
goto loop