@echo off

:menu
echo Shortcut List:
echo 1 - Exit
echo 2 - vlc
echo 3 - ...
choice /n /c:123 /M "Choose an option "
GOTO LABEL-%ERRORLEVEL%


:LABEL-2

echo "FullScreen: f; Skip: Alt-L/R, Ctrl-L/R, C-A-L/A; Volume: Ctrl-Up/Down;
echo "Mute: m; Slower/Faster: -/+; Hide Controls: Ctrl-h;"

PAUSE
goto menu


:LABEL-3

echo "... test ..."

PAUSE
goto menu


:LABEL-1

rem https://stackoverflow.com/a/7760618/1132766 - escaping quotes in bat files