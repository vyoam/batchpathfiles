@echo off
SETLOCAL EnableDelayedExpansion

rem https://superuser.com/questions/731467/command-line-option-to-open-chrome-in-new-window-and-move-focus https://askubuntu.com/questions/511438/what-are-all-of-the-command-line-options-for-google-chrome https://groups.google.com/a/chromium.org/forum/#!msg/chromium-discuss/2PLFNJnRmjY/XFnHsIGDCtwJ
rem https://serverfault.com/a/57652

if "%1"=="g" (
for /f "tokens=1,*" %%a in ("%*") do (
   set paramg=%%a
   set therest=%%b
   )
start /d "C:\Program Files (x86)\Google\Chrome\Application\" chrome --new-window --app="https://www.google.com/search?q=!therest!"
) else (
start /d "C:\Program Files (x86)\Google\Chrome\Application\" chrome --new-window --app="%*"
)

rem not using app as need to create new tabs; p.s. using at it doesn't trigger chrome reload session(?) when restart
rem "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --new-window "https://www.google.com/search?q=%*"