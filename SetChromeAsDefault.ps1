Add-Type -AssemblyName 'System.Windows.Forms'
Start-Process $env:windir\system32\control.exe -ArgumentList '/name Microsoft.DefaultPrograms /page pageDefaultProgram\pageAdvancedSettings?pszAppName=google%20chrome'
Sleep 2
[System.Windows.Forms.SendKeys]::SendWait("{TAB} {TAB}{TAB}")
# https://superuser.com/questions/1069346/how-to-automate-setting-chrome-as-default-browser-in-windows-10
# added to start up thru C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup
# made a shrotcut there for C:\Windows\System32\btes.exe C:\Users\aamat\__MY_DATA\Path_Files\SetChromeAsDefault.ps1
# p.s.: after office pc win 10 upgrade jul 2019, chrome has been staying default anyway; also the default apps dialog mechanism has changed