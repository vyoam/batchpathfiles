@echo off
setlocal

if [%1]==[] (
echo The path of workspace should be provided.
goto END )

set /p AREYOUSURE=This batch file assumes c:\code is a hard link not a directory. Please confirm (Y/[N])?
if /i "%AREYOUSURE%" NEQ "Y" goto END

echo Removing current hard link
rmdir c:\code
echo Removed current hard link
mklink /J c:\code %1

echo.
echo Restarting IIS...
iisreset
:END
endlocal

