@echo off

cmd /c "exit /b 10"

echo %errorlevel%

rem https://stackoverflow.com/questions/1113727/what-is-the-easiest-way-to-reset-errorlevel-to-zero

goto skip
setlocal
FOR /F "tokens=3" %%a in ('REG QUERY "HKLM\Software\Microsoft\Internet Explorer" /v Version ^| FIND "REG_SZ"') DO (
  SET v=%%a
)

echo %v%

FOR /F "tokens=1 delims=." %%m in ('echo %v%') do (
  SET m=%%m
)
echo %m%
endlocal
:skip