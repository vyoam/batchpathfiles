@echo off

rem todo: tgmerge - cud use base param to avoid load file prompt? also take care of whitespace prompt.
rem todo: wrt the errorlvele changes - cleanup
rem todo: cud use merge instead of diff in last step of option 2 (sno 3)

echo Current folder:
rem pwd

:menu
echo Custom git supplementary scripts:
echo 1 - Exit
echo 3 - gitkeep - create a patch and store in homedrive
echo 2 - save manual config; revert config; commit; pull; diff manual config files
echo 4 - diff manual config files
echo 5 - test
echo 6 - Fetch specified branch ref (esp. for non-current branch)
echo 7 - Fetch upto a commit to the specified branch ref (esp. for remotes/origin/master)
echo 8 - Go via TGit Browse Refs - to visually compare b/w branches/commits
echo 9 - git shallow cloen/fetch
choice /n /c:123456789 /M "Choose an option: "
GOTO LABEL-%ERRORLEVEL%


:LABEL-3
gitkeep
PAUSE
goto menu


:LABEL-2

rem https://stackoverflow.com/questions/4562210/how-to-open-the-tortoisegit-log-window-from-command-line
rem https://stackoverflow.com/questions/5139290/how-to-check-if-theres-nothing-to-be-committed-in-the-current-branch
rem https://git-scm.com/docs/git-diff#git-diff---exit-code
rem https://stackoverflow.com/questions/8055371/how-do-i-run-two-commands-in-one-line-in-windows-cmd

echo "save manual config; revert config; >>>"
for /f "tokens=*" %%a in (codeConfigEdits.txt) do (
echo  ---"%%a"--- ... ==="%%~pa\%%~nxa.configbackup"===
cmd /c "git diff --exit-code ^"%%a^""
if not errorlevel 1 (echo 0) else (echo 1)
rem errorlevel 0 means no diff found
if errorlevel 1 (copy "%%a" "%%~pa\%%~nxa.configbackup" && git checkout -- "%%a") 
)
echo "<<<"

echo "commit >>>"
TortoiseGitProc /command:commit /path:.
echo "<<<"

echo "pull >>>"
TortoiseGitProc /command:pull /path:.
echo "<<<"

echo "diff manual config files >>>"
for /f "tokens=*" %%a in (codeConfigEdits.txt) do  echo  ---"%%a"--- ... ==="%%~pa\%%~nxa.configbackup"===  && TortoiseGitMerge /theirs:"%%~pa\%%~nxa.configbackup" /mine:"%%a"
echo "<<<"

PAUSE
goto menu


:LABEL-4

echo "diff manual config files >>>"
for /f "tokens=*" %%a in (codeConfigEdits.txt) do  echo  ---"%%a"--- ... ==="%%~pa\%%~nxa.configbackup"===  && TortoiseGitMerge /theirs:"%%~pa\%%~nxa.configbackup" /mine:"%%a"
echo "<<<"

PAUSE
goto menu


:LABEL-5

cmd /c "exit /b 0"
echo %errorlevel%

for /f "tokens=*" %%a in (codeConfigEdits.txt) do (
cmd /c "git diff --exit-code ^"C:\CodeOnePlatform\paymentrestservice\payment-rest-service\src\main\resources\log4j.xml^""
echo %errorlevel%
if not errorlevel 1 (echo 0) else (echo 1)
)

echo %errorlevel% OUT OF LOOP

rem seems git diff doesn't update errorlevel if it was zero? so second last cmd's one (possibly non-zero) wud be retained even if diff didn't find any stuff
rem no its' issue with loop n %errorlevel% resolution

rem https://ss64.com/nt/errorlevel.html https://stackoverflow.com/questions/3942265/errorlevel-in-a-for-loop-batch-windows

cmd /c "exit /b 0"
echo %errorlevel%

for /f "tokens=*" %%a in (codeConfigEdits.txt) do (
cmd /c "git diff --exit-code ^"C:\CodeOnePlatform\paymentrestservice\payment-rest-service\src\main\resources\logging.properties^""
echo %errorlevel%
if not errorlevel 1 (echo 0) else (echo 1)
)

echo %errorlevel% OUT OF LOOP

pause
goto menu

:LABEL-6
echo "Fetch specified branch (esp. for non-current branch) >>>"
echo git fetch origin refs/heads/Authorize.Net_4.140.60_BLRSu2Fixes2020.Feature.Dev:refs/remotes/origin/Authorize.Net_4.140.60_BLRSu2Fixes2020.Feature.Dev
echo git fetch origin Authorize.Net_4.140.60_BLRSu2Fixes2020.Feature.Dev
echo git fetch origin master
echo "<<<"
PAUSE
goto menu

:LABEL-7
rem https://stackoverflow.com/questions/41249672/how-to-fetch-git-commits-by-small-batches
rem https://stackoverflow.com/questions/14872486/retrieve-specific-commit-from-a-remote-git-repository/14872590#14872590
echo "Fetch upto a commit to the specified branch ref (esp. for remotes/origin/master) >>>"
echo https://anettfs.visa.com:8443/AuthorizeNet/AuthorizeNet/_git/Authorize.Net/history?_a=history
echo git fetch origin 01904c5e727c9ddea132598826f2faf7afe6f769:refs/remotes/origin/master
echo "<<<"
PAUSE
goto menu

:LABEL-8
echo "--- >>>"
echo "<<<"
PAUSE
goto menu

:LABEL-9
echo "see comments"
echo "git clone https://stash.trusted.visa.com:7990/scm/cm/payment-notification-receiver.git --depth 1 --no-single-branch"
echo "git clone https://stash.trusted.visa.com:7990/scm/cm/payment-notification-receiver.git --depth 1 --branch feature/test123"
echo "git fetch origin develop --depth 10"
echo "git fetch --depth 10"
echo "https://stackoverflow.com/questions/17714159/how-do-i-undo-a-single-branch-clone"
PAUSE
goto menu

:LABEL-1

rem https://stackoverflow.com/a/7760618/1132766 - escaping quotes in bat files