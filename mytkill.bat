@echo off

set /p HANDLENAME=Provide substring for handle name (tomcat, usually)... 
handle -nobanner %HANDLENAME%

set /p PID=From the above output, select a PID to kill... 
taskkill /f /pid %PID%