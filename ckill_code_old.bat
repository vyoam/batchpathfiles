@echo off

rem https://stackoverflow.com/a/7486816
rem https://stackoverflow.com/questions/39731879/wmic-product-where-name-like-no-instances-available-if-run-in-batch-fil
rem https://forums.techguy.org/threads/solved-convert-a-string-to-a-number-with-a-batch-file.996034/ - cudn't leverage
rem https://stackoverflow.com/questions/13805187/how-to-set-a-variable-inside-a-loop-for-f

rem for each instance of chrome process, if parent process name is not chrome, kill
rem when original parent process has died, this doesn't work
rem cud just filter by cmd line of the chrome - like u do visually?

SetLocal EnableExtensions EnableDelayedExpansion

for /f "usebackq skip=1 tokens=1,2 delims= " %%a in (`wmic process where ^(executablepath like "%%chrome%%"^) get parentprocessid^, processid`) do call :Fn1 %%a %%b
goto End

:Fn1
for /f "usebackq skip=1 tokens=1 delims= " %%a in (`wmic process where ^(processid^=%1 and not executablepath like "%%chrome%%"^) get processid`) do call :Fn2 %2 %%a
goto :eof

:Fn2
if "%2" == "" (
echo Blank
) else (
echo Parent %2
wmic process where ^(processid=%2^) get executablepath, processid
echo Killing %1
wmic process where ^(processid=%1^) get executablepath, processid
taskkill /F /PID %1
exit
)
goto :eof


:End