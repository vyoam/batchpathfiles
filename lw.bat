@echo off

rem   scripting - How to convert the value of %USERNAME% to lowercase within a Windows batch script? - Stack Overflow
rem   https://stackoverflow.com/questions/284776/how-to-convert-the-value-of-username-to-lowercase-within-a-windows-batch-scrip

rem   How to convert a string to lower case in Bash? - Stack Overflow
rem   https://stackoverflow.com/questions/2264428/how-to-convert-a-string-to-lower-case-in-bash

rem   linux - Pass a (large) string to 'grep' instead of a file name - Super User
rem   https://superuser.com/questions/748724/pass-a-large-string-to-grep-instead-of-a-file-name

rem   linux - sed with literal string--not input file - Stack Overflow
rem   https://stackoverflow.com/questions/13055889/sed-with-literal-string-not-input-file

rem   HowTo: Use clip.exe to Redirect Command Line Output to the Windows Clipboard
rem   https://thebackroomtech.com/2007/09/14/howto-use-clipexe-to-redirect-command-line-output-to-the-windows-clipboard/

echo %1 |  tr '[:upper:]' '[:lower:]' | clip
