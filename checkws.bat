@echo off
setlocal

	FOR /F "tokens=6" %%a in ('dir c:\code? ^| FIND /I "junction"') DO (
		SET x=%%a
	)
	REM echo %x%

	FOR /F "tokens=1 delims=[]" %%b in ('echo %x%') do (
		SET y=%%b
	)
	echo %y% = Folder currently mapped to c:\code
	SET y=%y:\=/%
	
	echo Your workspaces mapped to %y% (across machines) ...
	FOR /F "tokens=1,3" %%a in ('accurev show wspaces ^| FIND /I "%y%"') DO (
		echo %%b		%%a
	)
	
endlocal