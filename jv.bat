@echo off
setlocal

if [%~n1]==[] (
set fileName=Runner
) else (
set fileName=%~n1
)

echo Compiling... %fileName%.java
javac %fileName%.java

echo Running... %fileName%
echo ^>^>^> ^>^>^> ^>^>^>
echo.
java %fileName%
echo.
echo ^<^<^< ^<^<^< ^<^<^<

:END
endlocal

