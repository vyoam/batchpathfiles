mygit
    
    gitkeep - creates a git patch against HEAD and stores in homedrive; in case u can't push to remote because of restrictions, this can be useful
    
    save manual config changes (stored in a repo-specific list of files) done, pull, reapply; a more controlled approach compared to stashing
    
utime

    show Unix/POSIX time - seconds since 1jan1970
    
acmd, anpp

    open cmd, npp in admin mode using Beyond Trust - Need certain shortcuts to be created
    
spath

    show the short path
    
folderstr

    copy file while re-creating folder structure  at destination
    
xcmd, acmd
    
    run the specified command and exit (useful to run from Run dialog)