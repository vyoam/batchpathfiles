rem @echo off

SetLocal EnableExtensions EnableDelayedExpansion

for /f "usebackq skip=1 tokens=1,2 delims= " %%a in (`wmic process where ^(executablepath like "%%%1%%"^) get processid`) do taskkill /F /PID %%a

:End